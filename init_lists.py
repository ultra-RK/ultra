from tqdm import tqdm
import os, sys, glob
import argparse
import numpy as np


################################################################################
def change_directory (path_lists, new_dir):
################################################################################
# change_directory
# change the path of the directory where the traces are stored assuming they
# are all stored in the same directory
#
# input:
#  + path_lists: file containing the lists
#  + new_dir: new directory
################################################################################
    [x_train_filelist, x_val_filelist, x_test_filelist, y_train, y_val, y_test] \
        = np.load (path_lists, allow_pickle = True)

    for i in range (len (x_train_filelist)):
        x_train_filelist [i] = new_dir +  '/' + os.path.basename (x_train_filelist [i])

    for i in range (len (x_val_filelist)):
        x_val_filelist [i] = new_dir +  '/' + os.path.basename (x_val_filelist [i])

    for i in range (len (x_test_filelist)):
        x_test_filelist [i] = new_dir +  '/' + os.path.basename (x_test_filelist [i])

    np.save (path_lists,
             np.array ([x_train_filelist, x_val_filelist, x_test_filelist, y_train, y_val, y_test],
                       dtype = object),
             allow_pickle = True)


################################################################################
def change_directory_learning (path_lists, new_dir):
################################################################################
# change_directory
# change the path of the directory where the learning (+ validating) traces are
# stored
#
# input:
#  + path_lists: file containing tthe lists
#  + new_dir: new directory
################################################################################
    [x_train_filelist, x_val_filelist, x_test_filelist, y_train, y_val, y_test] \
        = np.load (path_lists, allow_pickle = True)

    for i in range (len (x_train_filelist)):
        x_train_filelist [i] = new_dir +  '/' + os.path.basename (x_train_filelist [i])


    for i in range (len (x_val_filelist)):
        x_val_filelist [i] = new_dir +  '/' + os.path.basename (x_val_filelist [i])

    np.save (path_lists,
             np.array ([x_train_filelist, x_val_filelist, x_test_filelist, y_train, y_val, y_test],
                       dtype = object),
             allow_pickle = True)


################################################################################
def change_directory_testing (path_lists, new_dir):
################################################################################
# change_directory
# change the path of the directory where the testing traces are stored
#
# input:
#  + path_lists: file containing tthe lists
#  + new_dir: new directory
################################################################################
    [x_train_filelist, x_val_filelist, x_test_filelist, y_train, y_val, y_test] \
        = np.load (path_lists, allow_pickle = True)

    for i in range (len (x_test_filelist)):
        x_test_filelist [i] = new_dir +  '/' + os.path.basename (x_test_filelist [i])

    np.save (path_lists,
             np.array ([x_train_filelist, x_val_filelist, x_test_filelist, y_train, y_val, y_test],
                       dtype = object),
             allow_pickle = True)

################################################################################
if __name__ == '__main__':
################################################################################

    parser = argparse.ArgumentParser ()

    parser.add_argument ('--MIPS', type = str,
                         default = 'traces_MIPS/',
                         dest = 'path_mips',
                         help = 'Absolute path to the traces_MIPS directory')

    parser.add_argument ('--MIPS_relocated', type = str,
                         default = 'traces_MIPS_reloacted/',
                         dest = 'path_mips_relo',
                         help = 'Absolute path to the traces_MIPS_relocation directory')


    parser.add_argument ('--MIPS_cheap', type = str,
                         default = 'traces_MIPS_relocated_cheap/',
                         dest = 'path_mips_cheap',
                         help = 'Absolute path to the traces_MIPS_relocation_cheap directory')

    parser.add_argument ('--ARM', type = str,
                         default = 'traces_ARM/',
                         dest = 'path_arm',
                         help = 'Absolute path to the traces_ARM directory')

    args, unknown = parser.parse_known_args ()
    assert len (unknown) == 0, f"[WARNING] Unknown arguments:\n{unknown}\n"

    ## set the write path to the ARM traces
    l = glob.glob ('lists_ARM/**/*.npy',  recursive = True)

    for i in tqdm (range (len (l)), desc='init. lists of ARM traces'):
        change_directory (l [i], args.path_arm)

    l = glob.glob ('lists_MIPS/**/*.npy',  recursive = True)

    for i in tqdm (range (len (l)), desc='init. lists of MIPS traces'):

        if ('normal_to_relocated-cheap' == l [i].split ('/')[-1].split ('.')[0]):
            change_directory_learning (l [i], args.path_mips)
            change_directory_testing (l [i], args.path_mips_cheap)
        elif ('normal_to_relocated' == l [i].split ('/')[-1].split ('.')[0]):
            change_directory_learning (l [i], args.path_mips)
            change_directory_testing (l [i], args.path_mips_relo)
        elif ('relocated-cheap_to_relocated-cheap' == l [i].split ('/')[-1].split ('.')[0]):
            change_directory (l [i], args.path_mips_cheap)
        elif ('relocated_to_relocated' == l [i].split ('/')[-1].split ('.')[0]):
            change_directory (l [i], args.path_mips_relo)
        else:
            change_directory (l [i], args.path_mips)
