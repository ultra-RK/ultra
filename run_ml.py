import sys, os, glob
sys.path.insert(1, 'codes/')

from ml_evaluation  import evaluate


import numpy as np
import concurrent.futures



from tqdm import tqdm
import os, sys, glob
import argparse
import numpy as np


################################################################################
if __name__ == '__main__':
################################################################################


    parser = argparse.ArgumentParser()

    parser.add_argument ('--paths',  nargs = '+', default = [],
                        dest='paths',
                        help='Absolute paths to the list files or directories that contain the lists '
                         +' (could be used multiple times to give more than one list)')

    parser.add_argument("--mean_size", type = int,
                        nargs = '+', dest = 'mean_sizes',
                        help = 'Size of each means')

    parser.add_argument('--log-file', default = 'log-evaluation.txt',
                         dest = 'log_file',
                         help = 'Absolute path to the file to save results')


    parser.add_argument ('--acc', action='store', type=str,
                         dest='path_acc',
                         help='Absolute path of the accumulators directory')

    ## /!\ test tp speed-up loading the data only once
    parser.add_argument ('--nb_of_bandwidth', nargs = '+',
                         dest='nb_of_bandwidth', type = int,
                         # default = [2, 4, 6, 8, 10, 12, 14, 18, 20, 22, 24, 26, 28],
                         help='number of bandwidth to extract (could test test more than one)')

    parser.add_argument ('--time_limit', action ='store', type = float, default = .5,
                         dest = 'time_limit',
                         help = 'percentage of time to concerve (from the begining)')

    parser.add_argument('--metric', action='store', type=str, default='nicv_max',
                        dest='metric', help='Metric to use for select bandwidth: {mean, max} ')

    parser.add_argument('--core_per_computations', action='store', type=int, default=6,
                        dest='core_per_computation',
                        help='Number of core to use to run 1 evaluation')

    parser.add_argument('--nb_of_parallel_computation', action='store', type=int, default=4,
                        dest='nb_of_parallel_computation',
                        help='Number of evaluation in the same time')

    parser.add_argument('--algo', action='store', type=str, default='kernelPCA',
                        dest='algo', help='Dimension reduction algorithm: kernelPCA, incPCA, LDA')

    parser.add_argument('--n_components', action='store', type=int, default=15,
                        dest='n_comp',
                        help='Number of components for the dimension reduction')



    ## from
    ## https://gist.github.com/vadimkantorov/37518ff88808af840884355c845049ea
    ## python argparse_dict_argument.py --algo_args kernel=linear --algo_args degree=4
    parser.add_argument('--algo_args', dest = 'algo_args',
                        action = type('', (argparse.Action, ),
                                      dict(__call__ = lambda a, p, n, v, o:
                                           getattr(n, a.dest).update(dict([v.split('=')])))),
                        default = {},
                        help = 'Additionnal arguments for the dimension reduction algorithm (take a look to sklearn website)')


    args, unknown = parser.parse_known_args ()
    assert len (unknown) == 0, f"[WARNING] Unknown arguments:\n{unknown}\n"

    if (args.nb_of_bandwidth is None):
        ## default value
        args.nb_of_bandwidth = [2, 4, 6, 8, 10, 12, 14, 18, 20, 22, 24, 26, 28]

    if (args.mean_sizes is None):
        # default value
        args.mean_sizes = []


    ## parsing th input path
    lists = []
    for i in range (len (args.paths)):
        if (os.path.isfile (args.paths [i])): # it is a list not a directory
            lists.append (args.paths [i])
        else : # it must be a directory
            lists += glob.glob (args.paths [i] + "/**/*.npy", recursive = True)


    args_threads = []
    nb_of_workers = args.nb_of_parallel_computation

    for i in range (len (lists)):

        args_threads.append ([lists [i],
                              args.mean_sizes,
                              args.nb_of_bandwidth,
                              args.path_acc,
                              args.time_limit,
                              args.metric,
                              args.core_per_computation,
                              args.algo,
                              args.n_comp,
                              args.algo_args])


    f = open (args.log_file, 'a')

    with tqdm (total = len (args_threads), desc='eval') as pbar:
        with concurrent.futures.ProcessPoolExecutor (max_workers = nb_of_workers) as executor:
        ## the * is needed otherwise it does not work

            futures = {executor.submit (evaluate, *arg): arg for arg in args_threads}
            for future in concurrent.futures.as_completed (futures):
                pbar.update (1)

                f.write (future.result ())
                del futures [future]

    f.close ()
