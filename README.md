# ULTRA: Ultimate Rootkit Detection over the Air
This is the repository of the paper ["ULTRA: Ultimate Rootkit Detection over the Air"]((https://dl.acm.org/doi/10.1145/3545948.3545962)) published at RAID-2022.

** /!\ A major update is plan over January 2023 /!\ **

# Demos
## Diamorphine rootkit detection scenario
[![](http://img.youtube.com/vi/74UILJZt9eA/0.jpg)](http://www.youtube.com/watch?v=74UILJZt9eA "Diamorphine rootkit detection scenario")

# Python 3.6+
ULTRA analysis framework is in python. To be able to run the analysis you need to install python 3.6+ and the required packages:

```
pip install -r requirements.txt
```

# Traces (extracted bandwidth) [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5902451.svg)](https://doi.org/10.5281/zenodo.5902451)

 
Data are available on [zenodo](https://zenodo.org/record/5902451): 

```
https://zenodo.org/record/5902451 
```


 The archive "<u>traces.tar.gz</u>" is contains the following directories:

- **traces_ARM:** extracted bandwidth of spectrograms from ARM EM-traces, 
    - probe type: H-Field Langer RF-U 5-2 
    - probe position: 0,

- **traces_MIPS:** extracted bandwidth of spectrograms from MIPS EM-traces, 
	- probe type: H-Field Langer RF-U 5-2,
	- probe position: 0,

- **traces_MIPS_relocation:** extracted bandwidth of spectrograms from ARM EM-traces, 
	- probe type: H-Field Langer RF-U 5-2, 
	- probe position: 1,

- **traces_MIPS_relocation_cheap:** extracted bandwidth of spectrograms from ARM EM-traces, 
	- probe type: hand crafted 
    - probe position: 2.

# Lists 
Each scenario comes with a list of files used during the training, the validating (for MLP) and the testing phasis. 
We provide all the lists used to obtain the results listed in the paper (tree view of lists directories below).

In order to update the data location inside the list with the traces you download on [zenodo](https://zenodo.org/record/5902451) you must 
run the following python script **init_lists.py**:

```
usage: init_lists.py [-h] [--MIPS PATH_MIPS] [--MIPS_relocated PATH_MIPS_RELO] [--MIPS_cheap PATH_MIPS_CHEAP] [--ARM PATH_ARM]

optional arguments:
  -h, --help                      show this help message and exit
  --MIPS PATH_MIPS                Absolute path to the traces_MIPS directory
  --MIPS_relocated PATH_MIPS_RELO Absolute path to the traces_MIPS_relocation directory
  --MIPS_cheap PATH_MIPS_CHEAP    Absolute path to the traces_MIPS_relocation_cheap directory
  --ARM PATH_ARM                  Absolute path to the traces_ARM directory

example:
	python init_lists --MIPS traces_mips/ --MIPS_relocated traces_mips_reloacation/ --MIPS_cheap traces_MIPS_relocation_cheap/ --ARM traces_ARM/

``` 

## Tree view of the lists directories:

```
################################################################################
#                                 << lists_ARM/ >>
################################################################################

########################################
lists_ARM/baits:
########################################
│── getdents.npy
│── keyemu.npy
│── key_sleep.npy
│── kill.npy
│── open.npy
│── readir.npy
│── read.npy
│── renameat.npy
│── stat.npy
│── tcp4_seq_show_fixed.npy
│── tcp4_seq_show.npy
│── write.npy

########################################
lists_ARM/detection:
########################################
│── from_adore.npy
│── from_adore_to_beurk.npy
│── from_adore_to_diamorphine.npy
│── from_adore_to_m0hamed.npy
│── from_adore_to_vlany.npy
│── from_beurk.npy
│── from_beurk_to_adore.npy
│── from_beurk_to_diamorphine.npy
│── from_beurk_to_m0hamed.npy
│── from_beurk_to_vlany.npy
│── from_diamorphine.npy
│── from_diamorphine_to_adore.npy
│── from_diamorphine_to_beurk.npy
│── from_diamorphine_to_m0hamed.npy
│── from_diamorphine_to_vlany.npy
│── from_m0hamed.npy
│── from_m0hamed_to_adore.npy
│── from_m0hamed_to_beurk.npy
│── from_m0hamed_to_diamorphine.npy
│── from_m0hamed_to_vlany.npy
│── from_vlany.npy
│── from_vlany_to_adore.npy
│── from_vlany_to_beurk.npy
│── from_vlany_to_diamorphine.npy
│── from_vlany_to_m0hamed.npy
│── to_adore.npy
│── to_beurk.npy
│── to_diamorphine.npy
│── to_m0hamed.npy
│── to_vlany.npy

########################################
lists_ARM/keyloggers:
########################################
│── from_kisni_to_maK_it_hwkb.npy
│── from_kisni_to_maK_it.npy
│── from_maK_it_to_kisni_hwkb.npy
│── from_maK_it_to_kisni.npy

########################################
lists_ARM/malwares:
########################################
│── kisni_hwkb-mal_key_sleep.npy
│── kisni-mal_keyemu.npy
│── maK_it_hwkb-mal_key_sleep.npy
│── maK_it-mal_keyemu.npy
│── rootkit-adore-mal_getdents.npy
│── rootkit-adore-mal_open.npy
│── rootkit-adore-mal_readir.npy
│── rootkit-adore-mal_tcp4_seq_show.npy
│── rootkit-adore-mal_tcp4_seq_show_fixed.npy
│── rootkit-beurk-mal_getdents.npy
│── rootkit-beurk-mal_open.npy
│── rootkit-beurk-mal_readir.npy
│── rootkit-beurk-mal_stat.npy
│── rootkit-beurk-mal_tcp4_seq_show.npy
│── rootkit-beurk-mal_tcp4_seq_show_fixed.npy
│── rootkit-diamorphine-mal_getdents.npy
│── rootkit-diamorphine-mal_kill.npy
│── rootkit-diamorphine-obed-mal_getdents.npy
│── rootkit-diamorphine-obed-mal_kill.npy
│── rootkit-m0hamed-mal_getdents.npy
│── rootkit-m0hamed-mal_open.npy
│── rootkit-m0hamed-mal_read.npy
│── rootkit-m0hamed-mal_write.npy
│── rootkit-m0hamed-obed-mal_getdents.npy
│── rootkit-m0hamed-obed-mal_open.npy
│── rootkit-m0hamed-obed-mal_read.npy
│── rootkit-m0hamed-obed-mal_write.npy
│── rootkit-vlany-mal_getdents.npy
│── rootkit-vlany-mal_open.npy
│── rootkit-vlany-mal_readir.npy
│── rootkit-vlany-mal_renameat.npy
│── rootkit-vlany-mal_stat.npy
│── rootkit-vlany-mal_tcp4_seq_show.npy
│── rootkit-vlany-mal_tcp4_seq_show_fixed.npy

########################################
lists_ARM/obed:
########################################
│── from_diamorphine_getdents.npy
│── from_diamorphine-obed_getdents.npy
│── from_m0hamed_getdents.npy
│── from_m0hamed-obed_getdents.npy

########################################
lists_ARM/quiet_vs_noisy:
########################################
│── m0hamed_obed_mal_write-noisy_to_quiet.npy
│── m0hamed_obed_mal_write-quiet_to_noisy.npy


################################################################################
#                                << lists_MIPS/ >>
################################################################################

########################################
lists_MIPS/baits:
########################################
│── getdents.npy
│── keyemu.npy
│── key_sleep.npy
│── kill.npy
│── open.npy
│── readir.npy
│── read.npy
│── renameat.npy
│── stat.npy
│── tcp4_seq_show_fixed.npy
│── tcp4_seq_show.npy
│── write.npy

########################################
lists_MIPS/detection:
########################################
│── from_adore.npy
│── from_adore_to_beurk.npy
│── from_adore_to_diamorphine.npy
│── from_adore_to_m0hamed.npy
│── from_adore_to_vlany.npy
│── from_beurk.npy
│── from_beurk_to_adore.npy
│── from_beurk_to_diamorphine.npy
│── from_beurk_to_m0hamed.npy
│── from_beurk_to_vlany.npy
│── from_diamorphine.npy
│── from_diamorphine_to_adore.npy
│── from_diamorphine_to_beurk.npy
│── from_diamorphine_to_m0hamed.npy
│── from_diamorphine_to_vlany.npy
│── from_m0hamed.npy
│── from_m0hamed_to_adore.npy
│── from_m0hamed_to_beurk.npy
│── from_m0hamed_to_diamorphine.npy
│── from_m0hamed_to_vlany.npy
│── from_vlany.npy
│── from_vlany_to_adore.npy
│── from_vlany_to_beurk.npy
│── from_vlany_to_diamorphine.npy
│── from_vlany_to_m0hamed.npy
│── to_adore.npy
│── to_beurk.npy
│── to_diamorphine.npy
│── to_m0hamed.npy
│── to_vlany.npy

########################################
lists_MIPS/kernels_learning:
########################################
│── rootkit-adore-mal_getdents_kernel_learning.npy
│── rootkit-adore-mal_open_kernel_learning.npy
│── rootkit-adore-mal_readir_kernel_learning.npy
│── rootkit-adore-mal_tcp4_seq_show_fixed_kernel_learning.npy
│── rootkit-adore-mal_tcp4_seq_show_kernel_learning.npy
│── rootkit-diamorphine-mal_getdents_kernel_learning.npy
│── rootkit-diamorphine-mal_kill_kernel_learning.npy
│── rootkit-diamorphine-obed-mal_getdents_kernel_learning.npy
│── rootkit-diamorphine-obed-mal_kill_kernel_learning.npy
│── rootkit-m0hamed-mal_getdents_kernel_learning.npy
│── rootkit-m0hamed-mal_open_kernel_learning.npy
│── rootkit-m0hamed-mal_read_kernel_learning.npy
│── rootkit-m0hamed-obed-mal_getdents_kernel_learning.npy
│── rootkit-m0hamed-obed-mal_open_kernel_learning.npy
│── rootkit-m0hamed-obed-mal_read_kernel_learning.npy
│── rootkit-m0hamed-obed-mal_write_kernel_learning.npy

########################################
lists_MIPS/kernels_testing:
########################################
│── rootkit-adore-mal_getdents_kernel_testing.npy
│── rootkit-adore-mal_open_kernel_testing.npy
│── rootkit-adore-mal_readir_kernel_testing.npy
│── rootkit-adore-mal_tcp4_seq_show_fixed_kernel_testing.npy
│── rootkit-adore-mal_tcp4_seq_show_kernel_testing.npy
│── rootkit-diamorphine-mal_getdents_kernel_testing.npy
│── rootkit-diamorphine-mal_kill_kernel_testing.npy
│── rootkit-diamorphine-obed-mal_getdents_kernel_testing.npy
│── rootkit-diamorphine-obed-mal_kill_kernel_testing.npy
│── rootkit-m0hamed-mal_getdents_kernel_testing.npy
│── rootkit-m0hamed-mal_open_kernel_testing.npy
│── rootkit-m0hamed-mal_read_kernel_testing.npy
│── rootkit-m0hamed-obed-mal_getdents_kernel_testing.npy
│── rootkit-m0hamed-obed-mal_open_kernel_testing.npy
│── rootkit-m0hamed-obed-mal_read_kernel_testing.npy
│── rootkit-m0hamed-obed-mal_write_kernel_testing.npy

########################################
lists_MIPS/keyloggers:
########################################
│── from_kisni_to_maK_it_hwkb.npy
│── from_kisni_to_maK_it.npy
│── from_maK_it_to_kisni_hwkb.npy
│── from_maK_it_to_kisni.npy

########################################
lists_MIPS/malwares:
########################################
│── kisni_hwkb-mal_key_sleep.npy
│── kisni-mal_keyemu.npy
│── maK_it_hwkb-mal_key_sleep.npy
│── maK_it-mal_keyemu.npy
│── rootkit-adore-mal_getdents.npy
│── rootkit-adore-mal_open.npy
│── rootkit-adore-mal_readir.npy
│── rootkit-adore-mal_tcp4_seq_show.npy
│── rootkit-adore-mal_tcp4_seq_show_fixed.npy
│── rootkit-beurk-mal_getdents.npy
│── rootkit-beurk-mal_open.npy
│── rootkit-beurk-mal_readir.npy
│── rootkit-beurk-mal_stat.npy
│── rootkit-beurk-mal_tcp4_seq_show.npy
│── rootkit-beurk-mal_tcp4_seq_show_fixed.npy
│── rootkit-diamorphine-mal_getdents.npy
│── rootkit-diamorphine-mal_kill.npy
│── rootkit-diamorphine-obed-mal_getdents.npy
│── rootkit-diamorphine-obed-mal_kill.npy
│── rootkit-m0hamed-mal_getdents.npy
│── rootkit-m0hamed-mal_open.npy
│── rootkit-m0hamed-mal_read.npy
│── rootkit-m0hamed-mal_write.npy
│── rootkit-m0hamed-obed-mal_getdents.npy
│── rootkit-m0hamed-obed-mal_open.npy
│── rootkit-m0hamed-obed-mal_read.npy
│── rootkit-m0hamed-obed-mal_write.npy
│── rootkit-vlany-mal_getdents.npy
│── rootkit-vlany-mal_open.npy
│── rootkit-vlany-mal_readir.npy
│── rootkit-vlany-mal_renameat.npy
│── rootkit-vlany-mal_stat.npy
│── rootkit-vlany-mal_tcp4_seq_show.npy
│── rootkit-vlany-mal_tcp4_seq_show_fixed.npy

########################################
lists_MIPS/obed:
########################################
│── from_diamorphine_getdents.npy
│── from_diamorphine-obed_getdents.npy
│── from_m0hamed_getdents.npy
│── from_m0hamed-obed_getdents.npy

########################################
lists_MIPS/quiet_vs_noisy:
########################################
│── m0hamed_obed_mal_write-noisy_to_quiet.npy
│── m0hamed_obed_mal_write-quiet_to_noisy.npy

########################################
lists_MIPS/relocation:
########################################
│── normal_to_relocated-cheap.npy
│── normal_to_relocated.npy
│── relocated-cheap_to_relocated-cheap.npy
│── relocated_to_relocated.npy

``` 

# Source-code
The source code for the analysis is stored in ```codes/```. Below we give the usage of available features.

## displayer.py  
This scripts can be used to diplay spectrograms. 

```
usage: displayer.py [-h] [--display_trace PATH_TRACE] [--display_lists PATH_LISTS] [--list_idx LIST_IDX] [--metric METRIC] [--extension EXTENSION] [--path_save PATH_SAVE]

optional arguments:
  -h, --help                  show this help message and exit
  --display_trace PATH_TRACE  Absolute path to the trace to display
  --display_lists PATH_LISTS  Absolute path to the list to display
  --list_idx LIST_IDX         Which list to display (all = -1, learning: 0, validating: 1, testing: 2)
  --metric METRIC             Applied metric for the display of set (mean, std)
  --path_save PATH_SAVE       Absolute path to save the figure (if None, display in pop'up)

```

## list_manipulation.py  
This script can be used to display and creat list for experiences.

```
usage: list_manipulation.py [-h] [--raw PATH_RAW [PATH_RAW ...]] [--tagmap PATH_TAGMAP] [--save PATH_SAVE] [--main-lists PATH_MAIN_LISTS] [--extension EXTENSION] [--log-level LOG_LEVEL] [--lists PATH_LISTS]

optional arguments:
  -h, --help                    Show this help message and exit
  --raw PATH_RAW [PATH_RAW ...] Absolute path to the raw data directory
  --tagmap PATH_TAGMAP          Absolute path to a file containing the tag map
  --save PATH_SAVE              Absolute path to a file to save the lists
  --main-lists PATH_MAIN_LISTS  Absolute path to a file containing the main lists
  --extension EXTENSION         Extension of the given raw traces (here only npy traces have been provided)
  --log-level LOG_LEVEL         Configure the logging level: DEBUG|INFO|WARNING|ERROR|FATAL
  --lists PATH_LISTS            Absolute path to a file containing lists

```

## ml_evaluation.py   
This script can be used to run individual (for one given list, for mor please use ```run_ml.py``` ) machine leaning classifications. 

```
usage: ml_evaluation.py [-h] [--lists PATH_LISTS] [--mean_size MEAN_SIZES [MEAN_SIZES ...]] [--log-file LOG_FILE] [--acc PATH_ACC] [--nb_of_bandwidth NB_OF_BANDWIDTH [NB_OF_BANDWIDTH ...]] [--time_limit TIME_LIMIT] [--metric METRIC] [--core CORE] [--algo ALGO] [--n_components N_COMP] [--algo_args ALGO_ARGS]

optional arguments:
  -h, --help                                                Show this help message and exit
  --lists PATH_LISTS                                        Absolute path to a file containing the lists
  --mean_size MEAN_SIZES [MEAN_SIZES ...]                   Size of each means
  --log-file LOG_FILE                                       Absolute path to the file to save results
  --acc PATH_ACC                                            Absolute path of the accumulators directory
  --nb_of_bandwidth NB_OF_BANDWIDTH [NB_OF_BANDWIDTH ...]   Number of bandwidth to extract (could test test more than one)
  --time_limit TIME_LIMIT                                   Percentage of time to concerve (from the begining)
  --metric METRIC                                           Metric to use for select bandwidth: {mean, max}
  --core CORE                                               Number of core to use for multithreading
  --algo ALGO                                               Dimension reduction algorithm: kernelPCA, incPCA, LDA
  --n_components N_COMP                                     Number of components for the dimension reduction
  --algo_args ALGO_ARGS                                     Additionnal arguments for the dimension reduction algorithm (take a look to sklearn website)

```

## nicv.py  
This script could be used to compute **NICV** [1] from one list.

[1] - S. Bhasin, J. Danger, S. Guilley and Z. Najm, "NICV: Normalized inter-class variance for detection of side-channel leakage," 2014 International Symposium on Electromagnetic Compatibility, Tokyo, 2014, pp. 310-313.

```
usage: nicv.py [-h] [--acc PATH_ACC] [--lists PATH_LISTS] [--plot PATH_TO_PLOT] [--scale SCALE] [--time_limit TIME_LIMIT] [--bandwidth_nb BANDWIDTH_NB] [--metric METRIC] [--log-level LOG_LEVEL]

optional arguments:
  -h, --help                   Show this help message and exit
  --acc PATH_ACC               Absolute path of the accumulators directory
  --lists PATH_LISTS           Absolute path to a file containing the main lists
  --plot PATH_TO_PLOT          Absolute path to save the plot
  --scale SCALE                Scale of the plotting: normal|log
  --time_limit TIME_LIMIT      Percentage of time to concerve (from the begining)
  --bandwidth_nb BANDWIDTH_NB  Display the nb of selected bandwidth, by default no bandwidth selected
  --metric METRIC              Metric used to select bandwidth: {nicv}_{mean, max}
  --log-level LOG_LEVEL        Configure the logging level: DEBUG|INFO|WARNING|ERROR|FATAL
```

## read_logs.py

This script could be used to read the script obtained by ```ml_evaluation.py``` or by ```run_ml```. 

```
usage: read_logs.py [-h] [--path PATH [PATH ...]] [--plot PATH_TO_PLOT] [--bin_malware]

optional arguments:
  -h, --help              Show this help message and exit
  --path PATH [PATH ...]  Absolute path to the log files (could be used multiple times to give more than one list)
  --plot PATH_TO_PLOT     Absolute path to save the plot
  --bin_malware           If the results must be display for malware in a binary context (TPR/TNR)
```

# Machine learning

To run machine learning classification, a main python script is available on the root of the git repo: ```run_ml.py```:

```
usage: run_ml.py [-h] [--paths PATHS [PATHS ...]] [--mean_size MEAN_SIZES [MEAN_SIZES ...]] [--log-file LOG_FILE] [--acc PATH_ACC] [--nb_of_bandwidth NB_OF_BANDWIDTH [NB_OF_BANDWIDTH ...]] [--time_limit TIME_LIMIT] [--metric METRIC] [--core_per_computations CORE_PER_COMPUTATION]
                 [--nb_of_parallel_computation NB_OF_PARALLEL_COMPUTATION] [--algo ALGO] [--n_components N_COMP] [--algo_args ALGO_ARGS]

optional arguments:
  -h, --help                                               Show this help message and exit
  --paths PATHS [PATHS ...]                                Absolute paths to the list files or directories that contain the lists (could be used multiple times to give more than one list)
  --mean_size MEAN_SIZES [MEAN_SIZES ...]                  Size of each means
  --log-file LOG_FILE                                      Absolute path to the file to save results
  --acc PATH_ACC                                           Absolute path of the accumulators directory
  --nb_of_bandwidth NB_OF_BANDWIDTH [NB_OF_BANDWIDTH ...]  Number of bandwidth to extract (could test test more than one)
  --time_limit TIME_LIMIT                                  Percentage of time to concerve (from the begining)
  --metric METRIC                                          Metric to use for select bandwidth: {mean, max}
  --core_per_computations CORE_PER_COMPUTATION             Number of core to use to run 1 evaluation
  --nb_of_parallel_computation NB_OF_PARALLEL_COMPUTATION  Number of evalualition in  the same time
  --algo ALGO                                              Dimension reduction algorithm: kernelPCA, incPCA, LDA
  --n_components N_COMP                                    Number of components for the dimension reduction
  --algo_args ALGO_ARGS                                    Additionnal arguments for the dimension reduction algorithm (take a look to sklearn website)

example: to run all ARM experiments
	python run_ml.py\  
					  --paths lists_ARM\
					  --mean_size 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15\
					  --log-file arm_results.txt\
					  --acc ARM_acc/\
					  --nb_of_bandwidth 1 2 3 4 5 6 7 8 9 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100\
					  --time_limit 0.5\
					  --metric max\
					  --core_per_computations 10\
					  --nb_of_parallel_computation 2\
					  --algo KernelPCA\
					  --n_components 15
``` 


