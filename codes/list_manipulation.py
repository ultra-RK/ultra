################################################################################
import argparse
import glob
import logging
import os
import random

import numpy as np
from sklearn.model_selection import train_test_split
from tabulate import tabulate


################################################################################
class bcolors:
################################################################################
# class bcolors
# use to get colors iun the terminal
#
################################################################################

    black='\033[30m'
    red='\033[31m'
    green='\033[32m'
    orange='\033[33m'
    blue='\033[34m'
    purple='\033[35m'
    cyan='\033[36m'
    lightgrey='\033[37m'
    darkgrey='\033[90m'
    lightred='\033[91m'
    lightgreen='\033[92m'
    yellow='\033[93m'
    lightyellow = '\u001b[33;1m'
    lightblue='\033[94m'
    pink='\033[95m'
    lightcyan='\033[96m'

    endc = '\033[0m'
    bold = '\033[1m'
    underline = '\033[4m'


    whitebg = '\u001b[47;1m'
    resetbg = '\u001b[0m'


################################################################################
def get_tag (f):
################################################################################
# get_tag
# [from neural-network/utils]
# get the tag from a filename
#
# input:
#  + f: list of files
#
# output:
# + tag
################################################################################

    c = f.count ("-")
    
    if (c == 2): # clean
        res = '-'.join (os.path.basename (f).split("-")[:-1])   
    else:
        res = '-'.join (os.path.basename (f).split("-")[:-2])
    
    return res
    

################################################################################
def display_tabular (table, header):
################################################################################
# display_tabular
# display the data as tabular
#
# inputs:
#  + table: tabular to display
#  + header: header of the tabular
################################################################################
    print(tabulate (table, headers= header))

################################################################################
def display_list (x_train, x_val, x_test, y_train, y_val, y_test):
################################################################################
# display_list
# display the the content of a list:
#
# inputs:
#  + x_{train, val, test} filenames to get the tags
#  + y_{train, val, test} labels of a list
################################################################################

    y_unique = np.unique (np.array (list (y_train) + list (y_val) + list (y_test)))
    tags = [np.array ([get_tag (f) for f in list (x_train)]),
            np.array ([get_tag (f) for f in list (x_val)]),
            np.array ([get_tag (f) for f in list (x_test)])]

    lines = []
    last_line = ['-', 'total', 0, 0, 0, 0, 0]

    y_idx_lines = []
    tags_idx_lines = []
    
    for i in range (len (y_unique)):
        line = [i, f'{bcolors.bold}{y_unique [i]}{bcolors.endc}']
        count = 0
        
        current_tags = []
        
        ## train 
        idx = np.where (np.array (y_train)  == y_unique [i])[0]
        line.append (len (idx))
        last_line [2] += len (idx)
        count += len (idx)
        
        current_tags.append (tags [0][idx])

        ## val
        idx = np.where (np.array (y_val)  == y_unique [i]) [0]
        line.append (len (idx))
        last_line [3] += len (idx)
        count += len (idx)

        current_tags.append (tags [1][idx])
                
        ## train + val
        line.append (count)
        last_line [4] += count
        
        ## test
        idx = np.where (np.array (y_test)  == y_unique [i]) [0]
        line.append (len (idx))
        last_line [5] += len (idx)
        count += len (idx)
        
        current_tags.append (tags [2][idx])
        
        ## totlal
        line.append (count)
        last_line [6] += count

        lines.append (line)
        y_idx_lines.append (len (lines) - 1)

        tmp_tags_idx_lines = []
        
        current_unique_tags = np.unique (np.concatenate ((current_tags [0], current_tags [1], current_tags [2])))

        for j in range (len (current_unique_tags)):
            
            line = [f'{i}-{j}', f'{current_unique_tags [j]}']
            tag_count = 0
            
            ## train 
            idx = np.where (np.array (tags [0])  == current_unique_tags [j])[0]
            line.append (len (idx))
            tag_count += len (idx)
        
            ## val
            idx = np.where (np.array (tags [1])  == current_unique_tags [j]) [0]
            line.append (len (idx))
                        
            ## train + val
            tag_count += len (idx)
            line.append (tag_count)
                    
            ## test
            idx = np.where (np.array (tags [2])  == current_unique_tags [j]) [0]
            line.append (len (idx))
                    
            ## totlal
            tag_count += len (idx)
            line.append (tag_count)

            lines.append (line)

            tmp_tags_idx_lines.append (len (lines) - 1)
        
        tags_idx_lines.append (tmp_tags_idx_lines)
        

    ## add the percentages
    lines_array = np.array (np.array (lines)[:, 2:], dtype = np.uint32)
    
    for i in range (lines_array.shape [1]): ## for each colunm
        for j in range (len (y_idx_lines)): ## for each label
            current_percentage = 100*lines_array [y_idx_lines [j], i]/lines_array [y_idx_lines, i].sum ()
            lines [y_idx_lines [j]][i + 2] = f'{bcolors.lightblue}{lines [y_idx_lines [j]][i + 2]}{bcolors.endc} {bcolors.lightgreen}[{current_percentage:.2f}]{bcolors.endc}'
            
            for k in range (len (tags_idx_lines [j])): # for each tags
                current_percentage_local = 100*lines_array [tags_idx_lines [j][k], i]/lines_array [tags_idx_lines [j], i].sum ()
                current_percentage_global = 100*lines_array [tags_idx_lines [j][k], i]/lines_array [y_idx_lines, i].sum ()

                lines [tags_idx_lines [j][k]][i + 2] = f'{bcolors.blue}{lines [tags_idx_lines [j][k]][i + 2]}{bcolors.endc}'\
                    +f' {bcolors.green}[{current_percentage_global:.2f}]{bcolors.endc}'\
                    +f' {bcolors.yellow}[{current_percentage_local:.2f}]{bcolors.endc}'
                

    for j in range (len (y_idx_lines)):
        lines [y_idx_lines [j]][0]  = f'{bcolors.whitebg} {lines [y_idx_lines [j]][0]} {bcolors.resetbg}'
                
    for i in range (2, len (last_line) - 1):
        last_line [i] = f'{bcolors.blue}{last_line [i]}{bcolors.endc} {bcolors.green}[{100*last_line [i]/last_line [-1]:.2f}]{bcolors.endc}'
        
    last_line [-1] = f'{bcolors.lightred}{last_line [-1]}{bcolors.endc}'

    lines.append (last_line)

    print(tabulate (lines, headers= [f'{bcolors.bold}idx{bcolors.endc}\nsub-idx',
                                     f'{bcolors.bold}label{bcolors.endc}\ntag',
                                     f'{bcolors.bold}train nbr [gobal %] {bcolors.endc}\nnbr [global %] [local %]',
                                     f'{bcolors.bold}val nbr [gobal %] {bcolors.endc}\nnbr [global %] [local %]',
                                     f'{bcolors.bold}test nbr [gobal %] {bcolors.endc}\nnbr [global %] [local %]',
                                     f'{bcolors.bold}total nbr [gobal %] {bcolors.endc}\nnbr [global %] [local %]'], tablefmt="grid"))



################################################################################
def parse_data (main_list, tagmap):
################################################################################
# parse_data
# [from neural-network/utils]
# generate a new list from a 'main_list' and a 'tagmap'.
# The labeling is based on the tagmap, that have the following organisation:
# [tag],[label],{1, 2 or 3}
# the number specify if the given [tag], that will have the [label], is used in
#   1: learning phase only,
#   2: testing phase only,
#   3: in both
# 
# inputs:
#  - filelist: list of the filename
#  - tagmap: tagmap used to label
#
# outputs:
#  - x_train_filelist, x_test_filelist, x_trainandtest_filelist: files lists
#  - y_train, y_test, y_trainandtest: labels
################################################################################

    ## read tagmap to get proportion, names and labels
    f = open (tagmap, 'r')
    lines = f.readlines ()
    f.close ()
    
    label = {}
    group = {}
    count = {}

    group_size = [0., 0.]
    change_percentages = False
    
    for line in lines:
        split = line.strip("\n").split(",")
       
        if (len (split) == 3):
            t, l, g = split
            change_percentages = True
            
        elif (len (split) == 4):
            t, l, g, c = split
            count [t] = [float (c)]

        else: ## it means 5, two percentages
            t, l, g, c0, c1 = split
            count [t] = [float (c0), float (c1)]

        g = int (g)
        label [t] = l
        group [t] = g

        if (g == 3):
            group_size [0] += 1
            group_size [1] += 1

        else:
            group_size [g - 1] += 1

    ## /!\ sanity check of the percentage (must exist)
    ## if at least one key as no count all we be made
    ## equi-distributed
    if (change_percentages):
        for k in label.keys ():
            if (group [k] == 3):
                count [k] = [100./group_size [0], 100./group_size [1]] 
            else:
                count [k] = [100./group_size [group [k] - 1]]


    ## load all file lists
    x_train, x_val, x_test, y_train, y_val, y_test = np.load (main_list, allow_pickle = True)
    
    ## get for each file in the 'learning' (train + val) his label, if tags are in the tagmaps
    current_list = np.concatenate ((x_train, x_val))
    X        = []
    Y_tags = []
    for f in current_list:
        tag = get_tag (f)

        if tag in group.keys (): ## is in the tagmap
            if (group [tag] == 1 or group [tag] == 3):
            # if ((len (count [tag]) == 1 and count [tag][0] == 1) \
            #     or (len (count [tag]) == 2)):
                X.append (f)
                Y_tags.append (tag)

    X = np.array (X)
    Y_tags = np.array (Y_tags)
    
                
    current_proba  = []
    current_tags   = []
    current_labels = []
    for k in group.keys (): ## /!\ will crash if no proba specified
        if (group [k] == 1 or group [k] == 3):
            current_proba.append (count [k][0])
            current_labels.append (label [k])
            current_tags.append (k)
        
    
            
    idx = np.argsort (current_proba)[::-1]
    current_proba  = np.array (current_proba)[idx]
    for i in range (1, len (current_proba)):
        current_proba [i] += current_proba [i - 1] 
    
    current_tags   = np.array (current_tags)[idx]
    current_labels = np.array (current_labels)[idx]

    new_X_learn = []
    new_Y_learn = []
    
    step_size = 10
    while (True):
        # print (len (new_X_learn), step_size)
        
        proba = 100*np.random.random ()

        idx = 0
        for p in current_proba:
            if (proba < p):
                break
            else:
                idx += 1

        idx_to_add = np.where (Y_tags == current_tags [idx])[0]
        
        
        if (len (idx_to_add) == 0):
            break  
        else:
            if (len (idx_to_add) < step_size):
                step_size = 1
                # print (len (new_X_learn), step_size, len (idx_to_add))

            # if (len (new_X_learn) == 0):
            #     new_X_learn = list (X [idx_to_add [:step_size]])
            # else:
            new_X_learn += list (X [idx_to_add [:step_size]])
            new_Y_learn += [current_labels [idx]]*step_size

            X = np.delete (X, idx_to_add [:step_size])
            Y_tags = np.delete (Y_tags, idx_to_add [:step_size])
            
            
    ## get for each file in the 'testing' his label, if tags are in the tagmaps
    current_list = x_test
    X        = []
    Y_tags   = []
    for f in current_list:
        tag = get_tag (f)

        if tag in group.keys (): ## is in the tagmap
            if (group [tag] == 2 or group [tag] == 3):
            # if ((len (count [tag]) == 1 and count [tag][0] == 2) \
            #     or (len (count [tag]) == 2)):
                X.append (f)
                Y_tags.append (tag)

    X = np.array (X)
    Y_tags = np.array (Y_tags)
    
                
    current_proba  = []
    current_tags   = []
    current_labels = []
    for k in group.keys (): ## /!\ will crash if no proba specified
        if (group [k] == 2 or group [k] == 3):
            current_proba.append (count [k][group [k]%2])
            current_labels.append (label [k])
            current_tags.append (k)
        
        
    idx = np.argsort (current_proba)[::-1]
    current_proba  = np.array (current_proba)[idx]
    for i in range (1, len (current_proba)):
        current_proba [i] += current_proba [i - 1]
        
    current_tags   = np.array (current_tags)[idx]
    current_labels = np.array (current_labels)[idx]

    new_X_test = []
    new_Y_test = []

    while (True):
        proba = 100*np.random.random ()

        idx = 0
        for p in current_proba:
            if (proba < p):
                break
            else:
                idx += 1

        idx_to_add = np.where (Y_tags == current_tags [idx])[0]

        if (len (idx_to_add) == 0):
            break
        else:
            new_X_test.append (X [idx_to_add [0]])
            new_Y_test.append (current_labels [idx])

            X = np.delete (X, idx_to_add [0])
            Y_tags = np.delete (Y_tags, idx_to_add [0])
            

    # creat the validation
    x_train, x_val, y_train, y_val\
            = train_test_split (new_X_learn, new_Y_learn, test_size=0.2)


    return x_train, x_val, new_X_test, y_train, y_val, new_Y_test
    

################################################################################
def compute_main_list (data, extension):
################################################################################
# compute_main_list
# [from neural-network/utils]
# labeled the given trace by the tag.
# traces are separated as follow:
#   - learning: 80% -> {80% learning, 20% validating} 
#   - testing: 20%
# inputs:
#  - data: path to the directory containing all data or a list
#  - extension: type of file in data
#
# outputs:
# - lists: {filelist, labels} x {learning, validating, testing}
################################################################################

    filelist = []
    ## in case some element must be be ignores
    forbiden_elements = [] # 'udf', 'xfs', 'x_tables', 'overlay', 'nfsd', 'ip_vs', 'cifs', 'br_netfilter', 'bluetooth', 'atm']

    for i in range (len (data)):    
        if (not type (data [i]) is list):
            tmp = glob.glob (data [i] + "/**/*.%s"%extension, recursive = True)
            for f in tmp:
                add = True
                for e in forbiden_elements:
                    if (e in f):
                        add = False
                if (add):   
                    filelist.append (f)
        else:
            for f in data [i]:
                add = True
                for e in forbiden_elements:
                    if (e in f):
                        add = False
                if (add):   
                    filelist.append (f)

    random.shuffle (filelist)
    
    # get labels
    y = np.array ([get_tag (f) for f in filelist])   
    
    ## split into learning /testing 
    x_train_filelist, x_test_filelist, y_train, y_test\
        = train_test_split (filelist, y, test_size = 0.2)

    ## split into learning/validating
    x_train_filelist, x_val_filelist, y_train, y_val\
            = train_test_split (x_train_filelist, y_train, test_size=0.2)


    return x_train_filelist, x_val_filelist, x_test_filelist,\
        y_train, y_val, y_test


################################################################################
if __name__ == '__main__':
################################################################################
    
    parser = argparse.ArgumentParser()

    parser.add_argument ('--raw', nargs = '+', default = [],
                         dest = 'path_raw',
                         help = 'Absolute path to the raw data directory')

    parser.add_argument ('--tagmap', action = 'store',
                         type = str, dest = 'path_tagmap',
                         help = 'Absolute path to a file containing the tag map')

    parser.add_argument ('--save', action = 'store',
                         type = str, dest = 'path_save',
                         help = 'Absolute path to a file to save the lists')

    parser.add_argument ('--main-lists', action = 'store',
                         type = str, dest = 'path_main_lists',
                         help = 'Absolute path to a file containing the main lists')

    parser.add_argument ('--extension', default='dat',
                         type = str, dest = 'extension',
                         help = 'extension of the raw traces ')

    parser.add_argument ('--log-level', default=logging.INFO,
                         type=lambda x: getattr (logging, x),
                         help = "Configure the logging level: DEBUG|INFO|WARNING|ERROR|FATAL")

    parser.add_argument ('--lists', action = 'store', type = str, default = None,
                         dest = 'path_lists',
                         help = 'Absolute path to a file containing lists')

    args, unknown = parser.parse_known_args ()
    assert len (unknown) == 0, f"[WARNING] Unknown arguments:\n{unknown}\n"

    logging.basicConfig (level = args.log_level)

    if (logging.root.level < logging.INFO):
        print ("argument:")
        for arg in vars(args):
            print (f"{arg} : {getattr (args, arg)}")
            
            
    ## creat main list
    if (len (args.path_raw) != 0):
        # split testing and learning
        x_train_filelist, x_val_filelist, x_test_filelist, y_train, y_val, y_test \
            = compute_main_list (args.path_raw, extension = args.extension)

        if (logging.root.level < logging.INFO):

            display_list (x_train_filelist, x_val_filelist, x_test_filelist, y_train, y_val, y_test)

        np.save (args.path_save,
                 np.array ([x_train_filelist, x_val_filelist, x_test_filelist, y_train, y_val, y_test],
                           dtype = object),
                 allow_pickle = True)

        exit ()

    ## creat list
    if (args.path_tagmap):
        x_train_filelist, x_val_filelist, x_test_filelist, y_train, y_val, y_test \
             = parse_data (args.path_main_lists, args.path_tagmap)

        if (logging.root.level < logging.INFO):

            display_list (x_train_filelist, x_val_filelist, x_test_filelist, y_train, y_val, y_test)

        np.save (args.path_save,
                 np.array ([x_train_filelist, x_val_filelist, x_test_filelist, y_train, y_val, y_test],
                           dtype = object),
                 allow_pickle = True)
    

        exit ()


    ## only display
    if (args.path_lists and logging.root.level < logging.INFO):

        x_train, x_val, x_test, y_train, y_val, y_test = np.load (args.path_lists, allow_pickle = True)
        
        display_list (x_train, x_val, x_test, y_train, y_val, y_test)

        exit ()
