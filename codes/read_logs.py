import argparse
import re
import sys
from io import StringIO

import matplotlib
import numpy as np
from tqdm import tqdm

## to avoid bug when it is run without graphic interfaces
try:
    matplotlib.use('GTK3Agg')
    import matplotlib.pyplot as plt
except ImportError:
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt

import matplotlib.patches as patches
import tabulate
from list_manipulation import bcolors

GRADIENT_COLOR = [bcolors.orange,
                  bcolors.lightyellow,
                  bcolors.yellow,
                  bcolors.lightgreen,
                  bcolors.green,
                  bcolors.lightblue,
                  bcolors.blue,
                  bcolors.lightred,
                  bcolors.red]

def get_color (val):
    threshold = 0.90
    if (val < threshold):
        return f'{GRADIENT_COLOR [0]}{100*val:.1f}{bcolors.endc}'
    else: 
        return f'{GRADIENT_COLOR [int ( (val - threshold)*(len (GRADIENT_COLOR) - 2) / (1 - threshold)) + 1]}{100*val:.2f}{bcolors.endc}'

################################################################################
class Exp:
################################################################################
# to store the data of an experience (1 call of evaluation)
################################################################################
    def __init__ (self):# (self, tag, means, nb_of_bd):

        ## for the "meta" data
        # example:
        # 02.09.2021 - 13:30:41
        # path_lists: lists/extracted_bd_files_lists_tagmaps=executable_classification.npy
        # log_file: log-evaluation.txt
        # model_lda: None
        # model_svm: None
        # model_nb: None
        # means: [2, 3, 4, 5, 6, 7, 9, 10]
        # nb_of_bd: 6
        # path_acc: acc_stft/
        # time_limit: 0.5
        # metric: nicv_max

        self.date          = None
        self.hour          = None
        self.path_lists    = None
        self.log_file      = None
        self.model_lda     = None
        self.model_svm     = None
        self.model_nb      = None
        self.means         = None
        self.nb_of_bd      = None
        self.path_acc      = None
        self.time_limit    = None
        self.metric        = None
        self.Q             = None
        self.algo          = None
        self.n_componenets = None
        self.labels        = []
        self.algo_args     = None

        self.DR_duration = None ## DR : Dimension Reduction
        self.NB_duration  = None
        self.SVM_duration = None

        self.means_vect  = None
        self.nb_of_means = None

        ## to store the results
        for i in ['DR', 'SVM', 'NB']:
            setattr (self, f'{i}_duration', None)

        for i in ['SVM', 'NB']:
            setattr (self, f'{i}_acc', [])

            for j in ['macro', 'weighted']:
                setattr (self, f'{i}_{j}_precision', [])
                setattr (self, f'{i}_{j}_recall', [])
                setattr (self, f'{i}_{j}_f1', [])

    def init_vectors (self):
        ## [2, -2] to remove blank '[' and ']'
        self.means_vect = [int (i) for i in self.means[2:-2].split (',')]
        self.nb_of_means = len (self.means_vect)

################################################################################
def read_log (path):
################################################################################
# read_log
# extract all usefull information from log
#
# input:
#  + path: location of the log
#
# output:
#  + res: list of Exp
################################################################################
    f = open (path, 'r')
    lines = f.readlines ()

    res = []
    new_item = False
    reading_data = True

    current_res = None
    reading_means = False
    
    for line in lines:
        ## start a new experience
        if (10*'-' in line):
            if (reading_data): # save
                new_item = True
                reading_data = False
                if (current_res):
                    res.append (current_res)

                current_res = Exp ()

            else :
                new_item = False
                reading_data = True

        ## reading the header
        elif (new_item):
            if (not current_res.date):
                tmp_split = line.split ('-')

                current_res.date = tmp_split [0]
                current_res.hour = tmp_split [1]
            else:
                tmp_split = line.split (':')
                if (tmp_split [0] == 'means'):
                    if ('None' in tmp_split [1] or '[]' in tmp_split [1]):
                        setattr (current_res, 'means',  None)
                        setattr (current_res, 'nb_of_means',  0)
                    else:
                        setattr (current_res, 'means',  tmp_split [1])
                        current_res.init_vectors ()
                        
                elif (tmp_split [0] == 'path_lists' and 'limit=' in  tmp_split [1]):
                    # get the number of traces in the learning set
                    Q = int (tmp_split [1].split ('_limit=')[-1].split ('.')[0])
                    setattr (current_res, 'Q', Q)
                    setattr (current_res, 'path_lists',  tmp_split [1].split ('_limit=')[0])

                else :
                     setattr (current_res, tmp_split [0],  tmp_split [1])

        ## reading the data
        elif (reading_data):
            tmp_split = line.split ()

            ## save the times
            # if (len (tmp_split) > 1
            # and tmp_split [0] == 'LDA'
            # and tmp_split [1] == '(compuation):'): # = {DR, NB, SVM} (computation)
            #     setattr (current_res, f'{tmp_split [0]}_duration', float (tmp_split [2]))

            ## Test {NB, SVM}
            # elif
            if (len (tmp_split) > 1 and (tmp_split [0] == 'Test')):
                  current_exp = tmp_split [1]
                  reading_means = False

            # {NB, SVM}{-}{mean}{val:}
            elif (len (tmp_split) > 1 and (tmp_split [0] == 'NB' or tmp_split [0] == 'SVM')):
                  current_exp = tmp_split [0]
                  reading_means = True

            # {NB, SVM}_acc
            elif (len (tmp_split) > 1 and tmp_split [0] == 'accuracy'):
                getattr (current_res, f'{current_exp}_acc').append (float (tmp_split [1]))

            # {NB, SVM}_{macro, weighted}_{precision, recall, f1}
            elif (len (tmp_split) > 1 and (tmp_split [0] == 'macro' or tmp_split [0] == 'weighted')):
                getattr (current_res, f'{current_exp}_{tmp_split [0]}_precision').append (float (tmp_split [2]))
                getattr (current_res, f'{current_exp}_{tmp_split [0]}_recall').append (float (tmp_split [3]))
                getattr (current_res, f'{current_exp}_{tmp_split [0]}_f1').append (float (tmp_split [4]))


            # individual precision and recall f1-score
            elif (len (tmp_split) == 5 and tmp_split [0] != 'compute'):
                if (not reading_means):
                    getattr (current_res, 'labels').append (tmp_split [0])
                    setattr (current_res, f'{current_exp}_{tmp_split [0]}_precision', [])                
                    setattr (current_res, f'{current_exp}_{tmp_split [0]}_recall', [])
                    setattr (current_res, f'{current_exp}_{tmp_split [0]}_f1', [])
                    setattr (current_res, f'{current_exp}_{tmp_split [0]}_support', [])

                getattr (current_res, f'{current_exp}_{tmp_split [0]}_precision').append (float (tmp_split [1]))                
                getattr (current_res, f'{current_exp}_{tmp_split [0]}_recall').append (float (tmp_split [2])) 
                getattr (current_res, f'{current_exp}_{tmp_split [0]}_f1').append (float (tmp_split [3])) 
                getattr (current_res, f'{current_exp}_{tmp_split [0]}_support').append (float (tmp_split [4])) 
                
    f.close ()

    ## add the last one
    res.append (current_res)

    return res

################################################################################
def display_results (exps, output, bin_malware):
################################################################################
# display_results
# display the results from a log
#
# input:
#  + exps: list of Exp (cf read_log
#  + output: path to the file to save the figure (if None, a pop'up will be open)
################################################################################

    unique_tags = np.unique (np.array ([i.path_lists for i in exps]))
    bds = np.unique (np.array ([int (i.nb_of_bd) for i in exps]))

    count = 0
    if (exps [0].means is not None): ## meaning
        means = exps [0].means_vect ## /!\ all exp must have the means
        means = [1] + means # add the non-averaged

        nb_max_of_means = max ([len (i.means_vect) for i in exps])
        means = []
        for i in exps:
            means += i.means_vect

        means = np.sort (np.unique (means))

        means_NB_BA = np.zeros ((nb_max_of_means + 1, len (unique_tags)))
        means_NB_TPR = np.zeros ((nb_max_of_means + 1, len (unique_tags)))
        means_NB_TNR = np.zeros ((nb_max_of_means + 1, len (unique_tags)))
        
        
        means_SVM_BA = np.zeros ((nb_max_of_means + 1, len (unique_tags)))
        means_SVM_TPR = np.zeros ((nb_max_of_means + 1, len (unique_tags)))
        means_SVM_TNR = np.zeros ((nb_max_of_means + 1, len (unique_tags)))
        
    else: # No means
        means = []

    tags = np.unique (np.array ([i.path_lists.split ('/')[-1] for i in exps]))

    tabular = []

    ## Construct the tabular
    for i in range (len (unique_tags)):
        current_label = ''
        oposit_label  = ''
        
        tmp_name = unique_tags [i]
        tmp_name = tmp_name.replace ('lists_', '')
        tmp_name = tmp_name.replace ('.npy', '')
        
        current_row = [f'{count}', f'{tmp_name}']
        count += 1
        current_nb = [0, 0]
        current_svm = [0, 0]

        for j in range (len (exps)):
            ## in case we have more than 2 classes (not a detection procedure: clean Vs mal)
            if (unique_tags [i] == exps [j].path_lists and not bin_malware):
                if (len (exps [j].NB_acc)> 0
                    and ((exps [j].NB_acc [0] > current_nb [0]) or (exps [j].NB_acc [0] == current_nb [0]
                                                                    and exps [current_nb [1]].nb_of_bd < exps [j].nb_of_bd))):
                    current_nb [0] = exps [j].NB_acc [0]
                    current_nb [1] = j

                    if (len (means) != 0):
                        means_NB_BA   [0, i] = current_nb [0]
                        for k in range (len (exps [j].means_vect)):
                            means_NB_BA  [k + 1, i] = max (means_NB_BA [k + 1, i], exps [j].NB_acc [k + 1])

                if (len (exps [j].SVM_acc) > 0
                    and ((exps [j].SVM_acc [0] > current_svm [0]) or (exps [j].NB_acc [0] == current_svm [0]
                                                                      and exps [current_svm [1]].nb_of_bd < exps [j].nb_of_bd))):
                    current_svm [0] = exps [j].SVM_acc [0]
                    current_svm [1] = j

                    if (len (means) != 0):
                        means_SVM_BA  [0, i] = current_svm [0]
                        for k in range (len (exps [j].means_vect)):
                            means_SVM_BA [k + 1, i] = max (means_SVM_BA [k + 1, i], exps [j].SVM_acc [k + 1])

            
            elif (unique_tags [i] == exps [j].path_lists):
                if (current_label == ''):
                    for k in getattr (exps [j], 'labels'):
                        if ('mal' in k or 'inf' in k or 'rootkit' in k):
                            current_label = k
                        else:
                            oposit_label = k
                
                tmp_precision_0 = getattr (exps [j], f'NB_{current_label}_recall')
                tmp_precision_1 = getattr (exps [j], f'NB_{oposit_label}_recall')
                metric = (tmp_precision_0 [0] + tmp_precision_1 [0])/2 
                
                if (len (tmp_precision_0)> 0
                    and  (metric > current_nb [0]
                          or (metric == current_nb [0]
                              and exps [current_nb [1]].nb_of_bd > exps [j].nb_of_bd))):
                    current_nb [0] = metric
                    current_nb [1] = j

                    if (len (means) != 0):
                        means_NB_BA   [0, i] = current_nb [0]
                        
                        means_NB_TPR  [0, i] = tmp_precision_0 [0]
                        means_NB_TNR  [0, i] = tmp_precision_1 [0]
                        
                        for k in range (len (exps [j].means_vect)):
                            means_NB_BA  [k + 1, i] = 0.5*(tmp_precision_0 [k + 1] +  tmp_precision_1 [k + 1])
                            means_NB_TPR  [k + 1, i] = tmp_precision_0 [k + 1]
                            means_NB_TNR  [k + 1, i] = tmp_precision_1 [k + 1]



                            # max (tmp_precision [k + 1], means_NB [k + 1, i])# max (means_NB [k + 1, i], exps [j].NB_acc [k + 1])

                tmp_precision_0 = getattr (exps [j], f'SVM_{current_label}_recall')
                tmp_precision_1 = getattr (exps [j], f'SVM_{oposit_label}_recall')
                metric = (tmp_precision_0 [0] + tmp_precision_1 [0])/2 
                
                
                if (len (tmp_precision_0) > 0
                    and (metric > current_svm [0] 
                          or (metric == current_nb [0]
                              and exps [current_nb [1]].nb_of_bd > exps [j].nb_of_bd))):
                    
                    current_svm [0] = metric # exps [j].SVM_acc [0]
                    current_svm [1] = j

                    if (len (means) != 0):
                        means_SVM_BA  [0, i] = current_svm [0]
                        means_SVM_TPR  [0, i] = tmp_precision_0 [0]
                        means_SVM_TNR  [0, i] = tmp_precision_1 [0]
                        
                        for k in range (len (exps [j].means_vect)):
                            means_SVM_BA  [k + 1, i] = 0.5*(tmp_precision_0 [k + 1] +  tmp_precision_1 [k + 1])
                            means_SVM_TPR  [k + 1, i] = tmp_precision_0 [k + 1]
                            means_SVM_TNR  [k + 1, i] = tmp_precision_1 [k + 1]

                            # max (tmp_precision [k + 1], means_SVM [k + 1, i])# max (means_SVM [k + 1, i], exps [j].SVM_acc [k + 1])


        if (not bin_malware):
            current_row += [# NB
                f'{get_color (current_nb [0])}', f'{exps [current_nb [1]].nb_of_bd}',
                f'{get_color (exps [current_nb [1]].NB_macro_precision [0])}/{get_color (exps [current_nb [1]].NB_weighted_precision [0])}',
                f'{get_color (exps [current_nb [1]].NB_macro_recall [0])}/{get_color (exps [current_nb [1]].NB_weighted_recall [0])}',
                f'{get_color (exps [current_nb [1]].NB_macro_f1 [0])}/{get_color (exps [current_nb [1]].NB_weighted_f1 [0])}',
                # SVM
                f'{get_color (current_svm [0])}', f'{exps [current_svm [1]].nb_of_bd}',
                f'{get_color (exps [current_svm [1]].SVM_macro_precision [0])}/{get_color (exps [current_svm [1]].SVM_weighted_precision [0])}',
                f'{get_color (exps [current_svm [1]].SVM_macro_recall [0])}/{get_color (exps [current_svm [1]].SVM_weighted_recall [0])}',
                f'{get_color (exps [current_svm [1]].SVM_macro_f1 [0])}/{get_color (exps [current_svm [1]].SVM_weighted_f1 [0])}']

            head = ['idx', 'exp', 'DR + NB', '#bd',
                    'precision (macro/weigh)', 'recall (macro/weigh)', 'f1 (macro/weigh)',
                    'DR + SVM', '#bd',
                    'precision (macro/weigh)', 'recall (macro/weigh)', 'f1 (macro/weigh)']
        else:
            tmp_NB = [getattr (exps [current_nb [1]], f'NB_{current_label}_recall')[0],
                      getattr (exps [current_nb [1]], f'NB_{oposit_label}_recall')[0]]
            
            tmp_SVM = [getattr (exps [current_svm [1]], f'SVM_{current_label}_recall')[0],
                       getattr (exps [current_svm [1]], f'SVM_{oposit_label}_recall')[0]]

            current_row += [
                # NB
                f'{get_color (exps [current_nb [1]].NB_acc [0])}',
                f'{exps [current_nb [1]].nb_of_bd}',
                f'{get_color (tmp_NB [0])}', f'{get_color (tmp_NB [1])}',
                ' ',
                # SVM
                f'{get_color (exps [current_nb [1]].SVM_acc [0])}',
                f'{exps [current_svm [1]].nb_of_bd}',
                f'{get_color (tmp_SVM [0])}', f'{get_color (tmp_SVM [1])}']
                
            head = ['idx', 'exp', 'DR + NB acc. ', '#bd', 'TPR', 'TNR', '', 'DR + SVM acc.', '#bd', 'TPR', 'TNR']

        tabular.append (current_row)

    ## to avoid problem with grep (if grep is used, the last row is ignored)
    tabular.append (['-']*len (tabular [0]))

    
    res_tabulate = tabulate.tabulate (tabular, headers= head, tablefmt= 'grid')# "latex_longtable")
    print(res_tabulate)
    

    fig, axs = plt.subplots (nrows = 1, ncols= 2, figsize = (16, 16), sharex=True, sharey=True)

    cl = ['blue', 'green', 'black', 'red']
    ll = ['solid', 'dotted', 'dashed', 'dashdot']
    if (len (means) != 0):
        for i in range (len (tags)):

            axs [0].plot ([1] + list (means), means_NB_BA [:, i],
                          label = 'BA ' + tags [i],
                          linestyle = ll [i%len (ll)],
                          color=cl [i%len (cl)])
            
            axs [0].grid (True)
            axs [0].set_ylabel ('BA')
            axs [0].set_title ('NB')
            axs [0].set_xticks ([1] + list (means [1::2]))
            axs [0].set_xticklabels (['1'] + [str (m) for m in means[1::2]])
            
            axs [1].plot ([1] + list (means), means_SVM_BA [:, i],
                          label = 'SVM ' + tags [i],
                          linestyle = ll [i%len (ll)],
                          color=cl [i%len (cl)])
            
            axs [1].grid (True)
            axs [1].set_ylabel ('BA')
            axs [1].set_title ('SVM')
            axs [1].set_xticks ([1] + list (means [1::2]))
            axs [1].set_xticklabels (['1'] + [str (m) for m in means[1::2]])
    
        handles, labels = axs [0].get_legend_handles_labels()
        fig.legend (handles, labels, loc='upper center', ncol=4)
       
        # add a big axis, hide frame
        fig.add_subplot(111, frameon=False)
        # hide tick and tick label of the big axis
        plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
        
        plt.xlabel ('Number of traces t per mean')
        
        ## to save in file
        if (output):
            plt.savefig (output, format = 'pdf')
        else:
            plt.draw()
            plt.show ()

        return tabular

    
################################################################################
if __name__ == '__main__':
################################################################################

    parser = argparse.ArgumentParser()

    parser.add_argument ('--path',  nargs = '+', default = [],
                        dest='path',
                        help='Absolute path to the log files'
                         +' (could be used multiple times to give more than one list)')

    parser.add_argument ('--plot', action = 'store', default = None,
                         type = str, dest = 'path_to_plot',
                         help = 'Absolute path to save the plot')

    parser.add_argument ('--bin_malware', action = 'store_true', default = False,
                         dest = 'bin_malware',
                         help = 'If the results must be display for malware in a binary context (TPR/TNR)')

    args, unknown = parser.parse_known_args ()
    assert len (unknown) == 0, f"[WARNING] Unknown arguments:\n{unknown}\n"
    
    ## read and parse the log file
    res = []
    for i in range (len (args.path)):
        display_results (read_log (args.path [i]), args.path_to_plot, args.bin_malware)
