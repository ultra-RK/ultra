import argparse
import glob
import logging
import os
################################################################################
import sys
import time
from datetime import datetime

import joblib
import matplotlib
################################################################################
import numpy as np
from joblib import parallel_backend
from list_manipulation import get_tag
# sys.path.append(os.path.join (os.path.dirname (__file__), "../pre-processings/"))
from nicv import compute_nicv
from sklearn.decomposition import PCA, FastICA, KernelPCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics import classification_report
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import LocalOutlierFactor
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC, OneClassSVM
from tqdm import tqdm

## to avoid bug when it is run without graphic interfaces
try:
    matplotlib.use('GTK3Agg')
    import matplotlib.pyplot as plt
except ImportError:
    # print ('Warning importing GTK3Agg: ', sys.exc_info()[0])
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt


    
################################################################################
def load_traces (files_list, bandwidth, time_limit):
################################################################################
# load_traces
# load all the traces listed in 'files_lists', the 2D-traces are flattened
#
# input:
#  + files_list: list of the filenames
#  + bandwidth: selected bandwidth
#  + time_limit: percentage of the time trace to concerve
#
# output:
#  + traces: array containing the traces (dimension: DxQ, D number of features,
#  Q number of samples)
################################################################################
    ## get dimension
    tmp_trace = np.load (files_list [0], allow_pickle = True)[-1][bandwidth, :]

    ## takes only half of the features
    D = int (tmp_trace.shape [1])# *time_limit)

    tmp_trace = tmp_trace [:, :D].flatten ()
    
    traces = np.zeros ((len (tmp_trace), len (files_list)), dtype = tmp_trace.dtype)
    traces [:, 0] = tmp_trace 

    for i in tqdm (range (1, traces.shape [1]), desc = 'loading traces', leave = False):
        traces [:, i] = np.load (files_list [i], allow_pickle = True)[-1][bandwidth, :D].flatten ()

    return traces

################################################################################
def mean_by_label (traces, tags, labels, mean_size):
################################################################################
# mean_by_label
# mean traces per label, it means the input traces are mean by batch of 'mean_size' 
# of the same label
# 
# input:
#  + traces: array of traces (DxQ)
#  + tags: unique Id per {malware} x {baits} (from the name of the file)
#  + labels: labels used for the classification
#  + mean_size: nbr of samples per mean
#
# output:
#  + traces: mean traces (dimension: DxQ, D number of features,
#  Q number of samples)
#  + labels: lbaels of the average traces
################################################################################

    unique_tags = np.unique (tags)
    tags_to_label = {}
    for i in unique_tags: # conversion from tag to label
        tags_to_label [i] = labels [np.where (tags == i)[0][0]]
        
    tmp_res = []
    tmp_labels = []
    count = 0

    for i in range (len (unique_tags)): 
        idx = np.where (tags == unique_tags [i])[0]

        for j in range (0, len (idx) - mean_size, mean_size):
            tmp_labels.append (tags_to_label [unique_tags [i]])
            current_res = 0.
            
            for k in range (mean_size):
                current_res += traces [:, idx [j + k]]
                    
            tmp_res.append (current_res/mean_size)

    return np.array (tmp_res, dtype = tmp_res [0][0].dtype).T, np.array (tmp_labels)

################################################################################
def evaluate (path_lists,
              mean_sizes,
              nb_of_bd,
              path_acc,
              time_limit,
              metric,
              core,
              algo,
              n_components,
              algo_args):
################################################################################
# mean_by_label
# compute the {algo} + BN and {algo} + SVM machine learning algorithm
# 
# input:
#  + path_lists: path of the lists 
#  + log_file: where the results are saved
#  + mean_sizes: numbers of mean sizes to try
#  + nb_of_bd: nb of frequency band to conserve
#  + path_acc: directory where acculmulators are
#  + time_limit: percentage of the trace (from the begining)
#  + metric: metric to use for the bandwidth selection
#  + algo: name of the dimension reduction algorithms
#            - 'kernelPCA',
#            - 'incPCA'
#            - LDA
#  + n_components: nb of components conserved (for [*]PCA)
################################################################################
    parallel_backend('threading', n_jobs = core)
    print (f'start : {path_lists}')
    
    # file_log = open (log_file, 'a')
    string_res = ''
        
    ## load the data
    ## get indexes
    nb_of_bd = np.sort (np.array (nb_of_bd))[::-1]
    t, f, nicv, bandwidth = compute_nicv (path_lists, path_acc, None,\
                                          bandwidth_nb = nb_of_bd [0],
                                          time_limit = time_limit,
                                          metric = metric)
          
    ## load lists
    [x_train_filelist, x_val_filelist, x_test_filelist, y_train, y_val, y_test] \
        = np.load (path_lists, allow_pickle = True)

    full_learning_traces = load_traces (x_train_filelist, bandwidth, time_limit)
    full_validating_traces = load_traces (x_val_filelist, bandwidth, time_limit)

    ## learning
    full_traces = np.zeros ((full_learning_traces.shape [0],\
                             full_learning_traces.shape [1] \
                             + full_validating_traces.shape [1]))

    full_traces [:, :full_learning_traces.shape [1]] = full_learning_traces 
    full_traces [:, full_learning_traces.shape [1]:] = full_validating_traces 
    
        
    learning_labels = y_train
    validating_labels = y_val

    labels = np.concatenate ((learning_labels, validating_labels))

    ## now and testing
    full_testing_traces = load_traces (x_test_filelist,  bandwidth, time_limit)
    testing_labels = y_test

    ## get dim because it is flattened
    D = int (full_traces.shape [0]/nb_of_bd [0])

    ## get tags to be able to mean
    testing_tags = np.array ([get_tag (f) for f in x_test_filelist])
    
    for nb in tqdm (range (len (nb_of_bd)), desc='evaluate per bd', leave = False):
        
        ## only use the number bandwidth
        traces = full_traces [-(D*nb_of_bd [nb]):, :]
        testing_traces = full_testing_traces [-(D*nb_of_bd [nb]):, :]
        
        ## logging exp in file
        today = datetime.now ()
        d1 = today.strftime ("%d.%m.%Y - %H:%M:%S")

        # file_log.write ('-'*80 + '\n')
        string_res += '-'*80 + '\n'

        # file_log.write (d1 + '\n')
        string_res += d1 + '\n'
    
        string_res = string_res\
            + 'path_lists: %s\n'%str (path_lists)\
            + 'log_file: None\n'\
            + 'model_lda: None\n'\
            + 'model_svm: None\n'\
            + 'model_nb: None\n'\
            + 'means: %s\n'%str (mean_sizes)\
            + 'nb_of_bd: %s\n'%str (nb_of_bd [nb])\
            + 'path_acc: %s\n'%str (path_acc)\
            + 'time_limit: %s\n'%str (time_limit)\
            + 'metric: %s\n'%str (metric)\
            + 'algo: %s\n'%str (algo)\
            + 'n_components: %s\n'%str (n_components)


        string_res += '-'*80 + '\n'

        ## projection
        t0 = time.time ()
        if (algo == 'kernelPCA'):
            clf = KernelPCA (n_components = n_components, **algo_args)
        elif (algo == 'incPCA'):
            clf = IncrementalPCA (n_components = n_components, **algo_args)
        else:
            clf = LinearDiscriminantAnalysis (**algo_args)  
                
        transformed_traces = clf.fit_transform (traces.T, list (labels))
        
        string_res += 'DR (compuation): %s seconds\n'%(time.time () - t0)
        
        ## learning on projection
        t0 = time.time ()
        
        gnb  = GaussianNB ()
        gnb.fit (transformed_traces, labels)
        
        string_res += 'NB (computation): %s seconds\n'%(time.time () - t0)
        
        t0 = time.time ()
        svc = SVC ()

        svc.fit (transformed_traces, labels)

        string_res += 'SVM (computation): %s seconds\n'%(time.time () - t0)
        
        ## no means
        ## projection LDA
        t0 = time.time ()
        X = clf.transform (testing_traces.T)
        
        string_res += 'transform (size: %s): %s seconds\n'%(str(testing_traces.shape), str (time.time () - t0))
        
        ## NB 
        t0 = time.time ()

        predicted_labels = gnb.predict (X)
        string_res += 'Test NB  (size: %s) [%s seconds]:\n'%(str (X.shape), str (time.time () - t0))
        
        string_res +=  f'{classification_report (list (testing_labels), predicted_labels, digits = 4, zero_division = 0)}'
        
        ## SVM
        t0 = time.time ()
        predicted_labels = svc.predict (X)
        

        string_res += 'Test SVM  (size: %s) [%s seconds]:\n'%(str (X.shape), str (time.time () - t0))
        string_res += f'{classification_report (list (testing_labels), predicted_labels, digits = 4, zero_division = 0)}'
            
        for mean_size in mean_sizes:
            string_res += 'compute with %s per mean\n'%mean_size
            
            X, y = mean_by_label (testing_traces, testing_tags, np.array (testing_labels), mean_size)
            
            ## LDA on means
            t0 = time.time ()
            X = clf.transform (X.T)
           
            string_res += 'transform (size: %s): %s seconds\n'%(str(testing_traces.shape), str (time.time () - t0))
            
            ## NB
            t0 = time.time ()
            predicted_labels = gnb.predict (X)
            string_res += f'NB - mean {mean_size}:\n {classification_report (list (y), predicted_labels, digits = 4, zero_division = 0)}'
            
            # SVM
            t0 = time.time ()
            predicted_labels = svc.predict (X)
            string_res += f'SVM - mean {mean_size}:\n {classification_report (list (y), predicted_labels, digits = 4, zero_division = 0)}' 
            
    return string_res
        
################################################################################
if __name__ == '__main__':
################################################################################

    parser = argparse.ArgumentParser()

    parser.add_argument ('--lists', action = 'store',
                         type = str, dest = 'path_lists',
                         help = 'Absolute path to a file containing the lists')

    parser.add_argument("--mean_size", 
                        nargs = '+', dest = 'mean_sizes', type = int,
                        help = 'Size of each means')
        
    parser.add_argument('--log-file', default = 'log-evaluation.txt',
                         dest = 'log_file',
                         help = 'Absolute path to the file to save results')

    
    parser.add_argument ('--acc', action='store', type=str,
                         dest='path_acc',
                         help='Absolute path of the accumulators directory')

    ## /!\ test tp speed-up loading the data only once
    parser.add_argument ('--nb_of_bandwidth', nargs = '+',
                         dest='nb_of_bandwidth',
                         type=int,
                         # default = [2, 4, 6, 8, 10, 12, 14, 18, 20, 22, 24, 26, 28],
                         help='number of bandwidth to extract (could test test more than one)')

    parser.add_argument ('--time_limit', action ='store', type = float, default = 1,
                         dest = 'time_limit',
                         help = 'percentage of time to concerve (from the begining)')

    parser.add_argument('--metric', action='store', type=str, default='nicv_max',
                        dest='metric', help='Metric to use for select bandwidth: {mean, max} ')

    parser.add_argument('--core', action='store', type=int, default=6,
                        dest='core',
                        help='Number of core to use for multithreading')

    parser.add_argument('--algo', action='store', type=str, default='kernelPCA',
                        dest='algo', help='Dimension reduction algorithm: kernelPCA, incPCA, LDA')

    parser.add_argument('--n_components', action='store', type=int, default=15,
                        dest='n_comp',
                        help='Number of components for the dimension reduction')

    ## from
    ## https://gist.github.com/vadimkantorov/37518ff88808af840884355c845049ea
    ## python argparse_dict_argument.py --algo_args kernel=linear --algo_args degree=4
    parser.add_argument('--algo_args', dest = 'algo_args',
                        action = type('', (argparse.Action, ),
                                      dict(__call__ = lambda a, p, n, v, o:
                                           getattr(n, a.dest).update(dict([v.split('=')])))),
                        default = {},
                        help = 'Additionnal arguments for the dimension reduction algorithm (take a look to sklearn website)')
    

    args, unknown = parser.parse_known_args ()
    assert len (unknown) == 0, f"[WARNING] Unknown arguments:\n{unknown}\n"
    
    if (args.nb_of_bandwidth is None):
        ## default value
        args.nb_of_bandwidth = [2, 4, 6, 8, 10, 12, 14, 18, 20, 22, 24, 26, 28]

    if (args.mean_sizes is None):
        # default value
        args.mean_sizes = [] # [2, 3, 4, 5, 6, 7, 9, 10]

    
    evaluate (args.path_lists,
              args.mean_sizes,
              args.nb_of_bandwidth,
              args.path_acc,
              args.time_limit,
              args.metric,
              args.core,
              args.algo,
              args.n_comp,
              args.algo_args)
